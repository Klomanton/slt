let button = document.getElementById('camera');
let front = false;
let videoContainer = document.querySelector('div.camera_result');
let startCamera = function () {
    var constraints = {audio: true, video: {facingMode: (front ? "user" : "environment")}};
    navigator.mediaDevices.getUserMedia(constraints).then(videoStream);
}

let cameraChoose = false;
function videoStream(mediaSream) {
    cameraChoose=true;
    videoContainer.innerHTML = "<video id='video' style='width:100%;height:90%' muted autoplay playsinline></video><div class=\"border\" style=\"    \">\n" +
        "                <button  id=\"takePic\" style=\"\"></button>\n" +
        "            </div>";
    let video = document.querySelector('video');
    document.getElementById('takePic').onclick = capture
    video.srcObject = mediaSream;
    const videoTracks = mediaSream.getVideoTracks();
    video.onloadedmetadata = function (e) {
        video.play();
    };
    video.play();
}

let img;
let currentWindow = 1;
function getBack(){
    if(currentWindow>1){
        backToScreen(currentWindow-1);
    }
}
capture = function(){
    let capture_result=document.querySelector('.painter');
    let canvas = document.getElementById('risovach');
    let video = document.getElementById('video');
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    canvas.getContext('2d').drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
    document.querySelector('.camera').classList.add('hide');
    document.querySelector('.camera').innerHTML='';
    capture_result.querySelector('.color_container').classList.add('hide');
    capture_result.querySelector('#drowSwitch').classList.add('hide');
    capture_result.classList.add('show');

}


let risovach = document.getElementById('risovach'),
    context = risovach.getContext('2d');
let color = 'black';
let isDrawing = false;
var erase = false;
var drowSwitcher = document.getElementById('drowSwitch');

function secondFrame(){
    currentWindow=2;
    document.querySelector('.start_view .top').classList.remove('hide');
    document.querySelector('.start_view').classList.remove('hide');
    document.querySelectorAll('.start_view .first').forEach(function(e){
        e.classList.add('hide');
    });
    document.querySelectorAll('.start_view .second').forEach(function(e){
        e.classList.remove('hide');
    });
    document.querySelectorAll('.start_view .third').forEach(function(e){
        e.classList.add('hide');
    });
}
var name,date;
let blue="rgb(190,204,230)",green="rgb(198,221,195)",pink="rgb(216,192,216)",orange="rgb(240,199,179)",yellow="yellow";
var AgeColors = {
    0:blue,
    1:blue,
    2:blue,
    3:blue,
    4:green,
    5:orange,
    6:pink,
    7:pink,
    8:yellow
};
var color_back = blue;
document.querySelectorAll('.start_view .second input').forEach(function(e){

})
function checkFields(){
    let through=true;
    document.querySelectorAll('.start_view .second input').forEach(function(e){
        e.closest('div').querySelector('span.command').classList.remove('error');
        if(!e.checkValidity()){
            e.closest('div').querySelector('span.command').classList.add('error');
            through=false;
        }
    });
    document.querySelector('.start_view select').closest('div').querySelector('span.command').classList.remove('error');
    document.querySelectorAll('.start_view select').forEach(function(e){
        if(e.value=='Tag'||e.value=='Monat'||e.value=='Jahr'){
            e.closest('div').querySelector('span.command').classList.add('error');
            through=false;
        }
    })
    if(through){
        name = document.querySelector('input[type="text"]').value;
        date = document.querySelector('select[name="year"]').value+'-'+document.querySelector('select[name="month"]').value+'-'+document.querySelector('select[name="day"]').value;
        screenThree();
    }
}
function backToScreen(number){
    switch (number) {
        case 1:
            location.reload();
            break;
        case 2:
            document.querySelector('.fourthscreen').classList.add('hide');
            secondFrame();
            break;
        case 3:
            document.querySelector('.fourthscreen').classList.add('hide');
            screenThree()
            break;
        case 4:
            load4Choose(null);
            break;

    }
}
var years = 0;
function screenThree(){
    currentWindow=3;
    document.querySelector('.start_view').classList.remove('hide');
    years = (new Date() - new Date(date) - (24*60*60*1000)) / (365*24*60*60*1000);
    years = parseInt(years);
    if(years>8) years=8;
    color_back = AgeColors[years];
    document.querySelectorAll('.start_view .first').forEach(function(e){
        e.classList.add('hide');
    });
    document.querySelectorAll('.start_view .second').forEach(function(e){
        e.classList.add('hide');
    });
    document.querySelectorAll('.start_view .third').forEach(function(e){
        e.classList.remove('hide');
    });
    document.querySelector('.start_view .top').classList.add('hide');
    let name_ = name;
    if(name_.indexOf(' ')!==-1){
        name_=name_.split(' ');
        name_=name_[0];
    }
    document.querySelector('.start_view .third .title').querySelector('span[data-id="name"]').innerHTML = name_
}
let themes = {
    'mag1':{theme:'mag',title:'Das mag’ ich gern:'},
    'mag2':{theme:'mag',title:'Das mach ich gern:'},
    'Lieblingsorte':{theme:'Lieblingsorte',title:'Da bin ich gern:'},
    'Freunde':{theme:'Freunde',title:'Meine Freunde, meine Freundinnen und ich:'},
    'Zeichen':{theme:'Zeichen',title:'Zeichen, Buchstaben, Zahlen und Symbole, die ich kenne:'},
    'Kritzeln':{theme:'Kritzeln',title:'Kritzeln und Schreiben:'},
};
let theme = "";
function load4Choose(e=null){
    currentWindow=4;
    document.querySelector('.painter').classList.remove('show');
    if(document.querySelector('.fourthscreen .choose_block'))
    document.querySelector('.fourthscreen .choose_block').classList.remove('hide');
    document.querySelector('.camera').classList.remove('show');
    if(document.querySelector('.camera .camera_result'))
    document.querySelector('.camera .camera_result').style='';
    if(e !==null){
        theme = e.target.dataset.theme;
    }
    document.querySelector('.start_view').classList.add('hide');
    document.querySelector('.fourthscreen').classList.remove('fixed');
    document.querySelector('.fourthscreen').classList.remove('hide');
    document.querySelector('.fourthscreen .title_block').querySelector('span[data-id="theme_text"]').innerHTML=themes[theme].title;
    document.querySelector('.fourthscreen .title_block').querySelector('span[data-id="name"]').innerHTML=name;
    document.querySelector('.fourthscreen .title_block').querySelector('span[data-id="age"]').innerHTML=years;
    document.querySelector('.fourthscreen .title_block').querySelector('span[data-id="date"]').innerHTML=new Date().toLocaleDateString();
    document.querySelector('.fourthscreen .title_block').style = "background:"+color_back;
}
function clear() {
    context.clearRect(0, 0, risovach.width, risovach.height);
    context.fillStyle='white';
    context.fillRect(0, 0, risovach.width, risovach.height);
    if(cameraChoose){
        document.querySelector('.camera').classList.remove('hide');
        document.querySelector('.camera').innerHTML="<div class=\"camera_result\" style=\"background:black\">\n" +
            "            <div class=\"border\">\n" +
            "                <button id=\"takePic\"></button>\n" +
            "            </div>\n" +
            "        </div>";
        videoContainer = document.querySelector('div.camera_result');
        chooseCamera();
    }
    return false;
}
function chooseCamera(){
    if(cameraChoose){
        document.querySelector('.camera').classList.remove('hide');
        document.querySelector('.camera').innerHTML="<div class=\"camera_result\" style=\"background:black\">\n" +
            "            <div class=\"border\">\n" +
            "                <button id=\"takePic\"></button>\n" +
            "            </div>\n" +
            "        </div>";
        videoContainer = document.querySelector('div.camera_result');
    }
    currentWindow=5;
    document.querySelector('.fourthscreen').classList.add('fixed');
    if(document.querySelector('.fourthscreen .choose_block'))
        document.querySelector('.fourthscreen .choose_block').classList.add('hide');
    let margin_top = document.querySelector('.fourthscreen').offsetHeight;
    document.querySelector('.camera').classList.add('show');
    document.querySelector('.camera .camera_result').style="margin-top: "+margin_top+'px';
    startCamera();
}
window.onload = function () {
    document.getElementById('start').onclick=secondFrame;
    document.getElementById('checkFields').onclick=checkFields;
    document.querySelectorAll('.button.theme').forEach(function(e){
        e.onclick=load4Choose
    });
    document.querySelector('.fourthscreen .paint').onclick=function(){
        currentWindow=5;
        //risovach.width = window.innerWidth;
        //risovach.height = window.innerHeight;
        document.querySelector('.fourthscreen').classList.add('fixed');
        document.querySelector('.fourthscreen .choose_block').classList.add('hide');
        document.querySelector('.painter').classList.add('show');

        //clear();
        console.log(cameraChoose);
        if(cameraChoose){
            document.querySelector('.camera').classList.remove('hide');
            console.log(document.querySelector('.camera').innerHTML);
            document.querySelector('.camera').innerHTML="<div class=\"camera_result\" style=\"background:black\">\n" +
                "            <div class=\"border\">\n" +
                "                <button id=\"takePic\"></button>\n" +
                "            </div>\n" +
                "        </div>";
            console.log(document.querySelector('.camera').innerHTML);
            videoContainer = document.querySelector('div.camera_result');
            //clear();
        }
        let must_clear=false;
        if(cameraChoose){
            must_clear=true;
        }
        cameraChoose=false;
        if(!cameraChoose){
            let capture_result=document.querySelector('.painter');
            capture_result.querySelector('.color_container').classList.remove('hide');
            capture_result.querySelector('#drowSwitch').classList.remove('hide');
            if(must_clear){
                clear();
            }
        }
    }
    document.querySelector('.fourthscreen .photo').onclick=function(){cameraChoose=true;clear();};
    risovach.width = window.innerWidth;
    risovach.height = window.innerHeight;
    risovach.onmousedown = startDrawing;
    risovach.onmouseup = stopDrawing;
    risovach.onmouseout = stopDrawing;
    risovach.onmousemove = draw;
    risovach.ontouchstart = startDrawing;
    risovach.ontouchend = stopDrawing;
    risovach.ontouchcancel = stopDrawing;
    risovach.ontouchmove = draw;
    risovach.style="width:100vw;height:"+window.innerHeight+"px;";
    let today = new Date();
    //let dd = today.getDate();
    //let mm = today.getMonth()+1; //January is 0!
    let yyyy = today.getFullYear()-1;
    let past = new Date().getFullYear()-7;
    let cur = past;

    while(cur<=yyyy){
        let option = document.createElement('option');
        option.innerHTML=cur;
        document.querySelector('select[name="year"]').append(option);
        cur++;
    }
    let start = 1,end = 31;
    cur = 1;
    while(cur<=end){
        let option = document.createElement('option');
        let tmp = cur;
        if(tmp<10){
            tmp = '0'+tmp;
        }
        option.innerHTML=tmp;
        document.querySelector('select[name="day"]').append(option);
        cur++;
    }
    start = 1;end = 12;
    cur = 1;
    while(cur<=end){
        let option = document.createElement('option');
        let tmp = cur;
        if(tmp<10){
            tmp = '0'+tmp;
        }
        option.innerHTML=tmp;
        document.querySelector('select[name="month"]').append(option);
        cur++;
    }
    $('select').each(function(){
        $(this).select2({
            //allowClear: true,
            minimumResultsForSearch: Infinity,
            placeholder:{
                id:'-1',
                text:this.name
            }
        });
    })

    /*if(dd<10) dd='0'+dd
    if(mm<10) mm='0'+mm

    today = yyyy+'-'+mm+'-'+dd;

    let dateInput =document.querySelector('input[type="date"]')
    dateInput.setAttribute('max',today);
    dateInput.setAttribute('min',past);
    dateInput.onchange = function(){
        if(this.checkValidity()){
            this.closest('div').querySelector('span.command').classList.remove('error');
        }
    }*/
    document.getElementById('clear').onclick = clear;
    clear();
    document.getElementById('save').onclick=save;
    let colors = document.querySelectorAll('.color');
    colors.forEach(function (e) {
        e.onclick = changeColor;
    });

    function changeColor(element) {
        colors.forEach(function (e) {
            e.classList.remove('choosen');
        });
        element.target.classList.add('choosen');
        color = element.target.dataset.color;
    }

    context.strokeStyle = color;
    drowSwitcher.onclick = switchErase;
    function switchErase() {
        erase = !erase;
        if (erase) {
            drowSwitcher.src = "./pen.png";
            document.querySelector('.color_container').classList.add('hide')
        } else {
            drowSwitcher.src = "./erase.png";
            document.querySelector('.color_container').classList.remove('hide');
        }
    }
    function startDrawing(e) {
        isDrawing = true;
        context.beginPath();
        let x, y;
        x = e.pageX || e.touches[0].pageX;
        y = e.pageY || e.touches[0].pageY;
        context.moveTo(x - risovach.offsetLeft, y - risovach.offsetTop);
        draw(e);
        return false;
    }

    function draw(e) {
        if (isDrawing == true &&!cameraChoose) {
            let x, y;
            x = e.pageX || e.touches[0].pageX;
            y = e.pageY || e.touches[0].pageY;
            context.lineTo(x, y);
            context.strokeStyle = color;
            if (erase) {
                context.lineWidth = 30;
                context.strokeStyle = 'white';
            }else{
                context.lineWidth = 1;
            }
            context.stroke();
        }
        return false;
    }

    function stopDrawing(e) {
        isDrawing = false;
        return false;
    }

    function save(){
        let doc = new jsPDF("p", "mm", "a4");
        let width = doc.internal.pageSize.width;
        let height = doc.internal.pageSize.height;
        let img;
        document.querySelectorAll('.painter .control').forEach(function(e){
            
            e.classList.add('hide');
        });
        document.querySelectorAll('.button_back').forEach(function(e){
            e.classList.add('hide');
        })
        html2canvas(document.querySelector('body'), {
            onrendered: function (canvas) {
                var imgageData = new Image();
                srcpath = canvas.toDataURL("image/png");
                imgageData.src=srcpath;
                doc.addImage(imgageData , 'PNG',  0, 0,width,height);

                doc.save(name+'_'+years+'_'+themes[theme].theme+'.pdf');
                document.querySelectorAll('.painer .control').forEach(function(e){
                    e.classList.remove('hide');
                });
                let div = document.createElement('div');
                div.style="position: fixed;\n" +
                    "    z-index: 100;\n" +
                    "    background: rgba(255, 255, 255, 0.5);\n" +
                    "    inset: 0px;\n" +
                    "    display: flex;\n" +
                    "    flex-direction: column;\n" +
                    "    justify-content: space-around;"
                div.innerHTML="<img src='./ok.png' style='    max-width: 60%;\n" +
                    "    margin: 0 auto;\n" +
                    "    vertical-align: middle;'>";
                document.body.appendChild(div);
                document.querySelectorAll('.button_back').forEach(function(e){
                    e.classList.remove('hide');
                })
                setTimeout(function(){location.reload();},1000);
            }
        });

        return false;
    }
}


